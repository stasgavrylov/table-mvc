(function() {
    document.addEventListener('DOMContentLoaded', function() {
        var ctrl = myApp.ctrl;
        var view = myApp.view;

        // Listen to 'Add User' clicks.
        document.getElementById('add').addEventListener('click', function() {
            if (document.forms.addUserForm)
                return false;
            ctrl.showAddUserForm();
        });

        // Listen to clicks on <table> icons.
        document.getElementById('userlist').addEventListener('click', function(e) {
            var target = e.target;
            var role = target.tagName === 'I' ? 
                       target.parentElement.getAttribute('data-role') : 
                       target.getAttribute('data-role');
            switch(role) {
                case 'create':
                    ctrl.createUser();
                    break;
                case 'edit':
                    ctrl.initEdit(target);
                    break;
                case 'update':
                    ctrl.updateUser(target);
                    break;
                case 'revert':
                    ctrl.revertEdit();
                    break;
                case 'delete':
                    ctrl.deleteUser(target);
                    break;
                case 'cancel':
                    view.removeAddUserForm();
                    break;
            }
        });
    });
})();