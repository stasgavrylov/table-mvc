(function(myApp, $, undefined) {

    if (!(myApp.view))
        myApp.view = {};
    var view = myApp.view;

    // Remove user row from the table.
    view.removeUserRow = function(row) {
        row.parentNode.removeChild(row);
    };


})(window.myApp = window.myApp || {}, jQuery);