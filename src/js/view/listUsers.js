(function(myApp, $, undefined) {

    if (!(myApp.view))
        myApp.view = {};
    var view = myApp.view;

    // Render list of provided user records.
    view.listUsers = function(records) {
        var tableBody = document.querySelector('#userlist tbody');
        var bodyHTML = '';

        // InnerHTML here for brevity.
        for (var rec in records) {
            rec = records[rec];
            bodyHTML += '<tr data-id="' + rec.id + '">';
            bodyHTML += '<td>' + rec.name + '</td>';
            bodyHTML += '<td>' + rec.email + '</td>';
            bodyHTML += '<td>' + rec.phone + '</td>';
            bodyHTML += '<td>' + rec.street + '</td>';
            bodyHTML += '<td>' + rec.city + '</td>';
            bodyHTML += '<td>' + rec.state + '</td>';
            bodyHTML += '<td>' + rec.zip + '</td>';
            bodyHTML += '<td><a href="#" title="Edit User" data-role="edit">';
            bodyHTML += '<i class="glyphicon glyphicon-pencil"></i></a>';
            bodyHTML += '<a href="#" title="Delete User" data-role="delete">';
            bodyHTML += '<i class="glyphicon glyphicon-remove"></i></a></td></tr>';
        }
        tableBody.innerHTML = bodyHTML;
    };


})(window.myApp = window.myApp || {}, jQuery);