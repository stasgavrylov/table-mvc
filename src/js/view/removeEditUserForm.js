(function(myApp, $, undefined) {

    if (!(myApp.view))
        myApp.view = {};
    var view = myApp.view;

    // Removed rendered form and <tr> with inputs.
    view.removeEditUserForm = function() {
        var formEl = document.forms.editUserForm;
        formEl.parentNode.removeChild(formEl);

        document.getElementById('add').disabled = false;
    };


})(window.myApp = window.myApp || {}, jQuery);