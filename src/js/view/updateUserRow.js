(function(myApp, $, undefined) {

    if (!(myApp.view))
        myApp.view = {};
    var view = myApp.view;

    // Update user row after edit or revert.
    view.updateUserRow = function(user) {
        var row = document.querySelector('tr.edit');
        var newRow = myApp.ctrl.createRow(user);

        var tableBody = document.querySelector('#userlist tbody');
        tableBody.replaceChild(newRow, row);
    };


})(window.myApp = window.myApp || {}, jQuery);