(function(myApp, $, undefined) {

    if (!(myApp.view))
        myApp.view = {};
    var view = myApp.view;

    // Render form for user creation.
    view.renderAddUserForm = function() {
        var form = document.createElement('form');
        form.id = 'addUserForm';
        var table = document.querySelector('#userlist');
        table.parentNode.insertBefore(form, table);

        document.getElementById('add').disabled = true;
        
        var tableBody = table.tBodies[0];
        var addUserFormHTML = tableBody.innerHTML;

        // InnerHTML here for brevity.
        addUserFormHTML += '<tr class="create">';
        addUserFormHTML += '<td><input class="form-control" name="name" form="addUserForm" type="text" required></td>';
        addUserFormHTML += '<td><input class="form-control" name="email" form="addUserForm" type="email" required></td>';
        addUserFormHTML += '<td><input class="form-control" name="phone" form="addUserForm" type="tel" pattern="[0-9+\-]*" required></td>';
        addUserFormHTML += '<td><input class="form-control" name="street" form="addUserForm" type="text" required></td>';
        addUserFormHTML += '<td><input class="form-control" name="city" form="addUserForm" type="text" required></td>';
        addUserFormHTML += '<td><input class="form-control" name="state" form="addUserForm" type="text" required></td>';
        addUserFormHTML += '<td><input class="form-control" name="zip" form="addUserForm" type="text" pattern="[0-9]*" required></td>';
        addUserFormHTML += '<td><a href="#" title="Confirm" data-role="create"><i class="glyphicon glyphicon-ok"></i></a>';
        addUserFormHTML += '<a href="#" title="Cancel" data-role="cancel"><i class="glyphicon glyphicon-ban-circle"></i></a>';
        addUserFormHTML += '</td></tr>';

        tableBody.innerHTML = addUserFormHTML;
    };


})(window.myApp = window.myApp || {}, jQuery);