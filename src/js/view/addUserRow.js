(function(myApp, $, undefined) {

    if (!(myApp.view))
        myApp.view = {};
    var view = myApp.view;

    // Add new user row to the table.
    view.addUserRow = function(userRecord) {
        var tableBody = document.querySelector('#userlist tbody');
        var row = myApp.ctrl.createRow(userRecord);
        /*var row = document.createElement('tr');
        var rowHTML = '';

        // InnerHTML here for brevity.
        rowHTML += '<td>' + userRecord.name + '</td>';
        rowHTML += '<td>' + userRecord.email + '</td>';
        rowHTML += '<td>' + userRecord.phone + '</td>';
        rowHTML += '<td>' + userRecord.street + '</td>';
        rowHTML += '<td>' + userRecord.city + '</td>';
        rowHTML += '<td>' + userRecord.state + '</td>';
        rowHTML += '<td>' + userRecord.zip + '</td>';
        rowHTML += '<td><a href="#" title="Edit User" data-role="edit">';
        rowHTML += '<i class="glyphicon glyphicon-pencil"></i></a>';
        rowHTML += '<a href="#" title="Delete User" data-role="delete">';
        rowHTML += '<i class="glyphicon glyphicon-remove"></i></a></td>';

        row.setAttribute('data-id', userRecord.id);
        row.innerHTML = rowHTML;*/
        tableBody.appendChild(row);
    };


})(window.myApp = window.myApp || {}, jQuery);