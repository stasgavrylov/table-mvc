(function(myApp, $, undefined) {

    if (!(myApp.view))
        myApp.view = {};
    var view = myApp.view;

    // Removed rendered form and <tr> with inputs.
    view.removeAddUserForm = function() {
        var formEl = document.forms.addUserForm;
        formEl.parentNode.removeChild(formEl);

        var row = document.querySelector('tr.create');
        row.parentNode.removeChild(row);

        document.getElementById('add').disabled = false;
    };


})(window.myApp = window.myApp || {}, jQuery);