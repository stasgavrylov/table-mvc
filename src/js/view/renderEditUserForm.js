(function(myApp, $, undefined) {

    if (!(myApp.view))
        myApp.view = {};
    var view = myApp.view;

    // Change user row to edit user form.
    view.renderEditUserForm = function(userRecord, id) {
        var form = document.createElement('form');
        form.id = 'editUserForm';
        var table = document.querySelector('#userlist');
        var tableBody = table.tBodies[0];

        // Create new row element.
        var row = document.querySelector('tr[data-id="' + id + '"]');
        var newRow = document.createElement('tr');
        newRow.className = 'edit';
        newRow.setAttribute('data-id', id);

        // InnerHTML here for brevity.
        var newRowHTML = '';
        newRowHTML += '<td><input class="form-control" name="name" form="editUserForm" type="text" value="' + userRecord.name + '" required></td>';
        newRowHTML += '<td><input class="form-control" name="email" form="editUserForm" type="email" value="' + userRecord.email + '" required></td>';
        newRowHTML += '<td><input class="form-control" name="phone" form="editUserForm" type="tel" pattern="[0-9+\-]*" value="' + userRecord.phone + '" required></td>';
        newRowHTML += '<td><input class="form-control" name="street" form="editUserForm" type="text" value="' + userRecord.street + '" required></td>';
        newRowHTML += '<td><input class="form-control" name="city" form="editUserForm" type="text" value="' + userRecord.city + '" required></td>';
        newRowHTML += '<td><input class="form-control" name="state" form="editUserForm" type="text" value="' + userRecord.state + '" required></td>';
        newRowHTML += '<td><input class="form-control" name="zip" form="editUserForm" type="text" pattern="[0-9]*" value="' + userRecord.zip + '" required></td>';
        newRowHTML += '<td><a href="#" title="Update" data-role="update"><i class="glyphicon glyphicon-ok"></i></a>';
        newRowHTML += '<a href="#" title="Cancel" data-role="revert"><i class="glyphicon glyphicon-ban-circle"></i></a></td>';

        newRow.innerHTML = newRowHTML;

        // Append new elements to the view.
        table.parentNode.insertBefore(form, table);
        tableBody.replaceChild(newRow, row);

        // Disable 'Add User' button.
        document.getElementById('add').disabled = true;
    };


})(window.myApp = window.myApp || {}, jQuery);