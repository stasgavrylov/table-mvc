(function(myApp, $, undefined) {

    if (!(myApp.model))
        myApp.model = {};
    var model = myApp.model;

    // Create new user record.
    model.UserRecord = function(userdata) {
        var keys = Object.keys(userdata);
        var key;
        for (var i = 0; i < keys.length; i++) {
            key = keys[i];
            this[key] = userdata[key];
        }
    };

    // Property to store user record objects.
    model.UserRecord.instances = {};

    // Load test app data.
    model.loadTestData = function() {
        var data = {
            1: {
                "id": 1,
                "name": "Stas Gavrylov",
                "email": "stasgavrylov@gmail.com",
                "phone": "18294525813",
                "street": "Palma Cana",
                "city": "Bavaro",
                "state": "DR",
                "zip": "23000"
            },
            2: {
                "id": 2,
                "name": "Bill Murray",
                "email": "billmurray@yahoo.com",
                "phone": "18003332211",
                "street": "Hollywood",
                "city": "Los Angeles",
                "state": "US",
                "zip": "90028"
            }
        };
        model.testdata = JSON.stringify(data);
    };

    // Load all records.
    model.loadRecords = function() {
        var records;
        try {
            if (localStorage.getItem('userRecords'))
                records = JSON.parse(localStorage.getItem('userRecords'));
            else
                records = JSON.parse(this.testdata);

            for (var rec in records) {
                rec = records[rec];
                model.UserRecord.instances[rec.id] = rec;
            }
            console.log(Object.keys(records).length + ' records loaded.');
            return model.UserRecord.instances;
        }
        catch (e) {
            alert('Error when loading data: ' + e);
        }
    };

    // Save data to localStorage.
    model.saveToLocalStorage = function() {
        var recordsString = JSON.stringify(model.UserRecord.instances);
        localStorage.setItem('userRecords', recordsString);
    };

    // Save user record.
    model.saveRecord = function(userRecord) {
        model.UserRecord.instances[userRecord.id] = userRecord;
        model.saveToLocalStorage();
        return model.UserRecord.instances;
    };

    // Delete user record.
    model.deleteRecord = function(userRecordId) {
        delete model.UserRecord.instances[userRecordId];
        model.saveToLocalStorage();
    };

    // Store current edited record.
    model.storeEditedRecord = function(userRecord) {
        this.lastEditedRecord = userRecord;
    };

})(window.myApp = window.myApp || {}, jQuery);