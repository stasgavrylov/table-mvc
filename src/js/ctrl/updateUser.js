(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // Update user record.
    ctrl.updateUser = function(target) {
        if (ctrl.validateForm('editUserForm')) {
            var inputs = document.forms.editUserForm.elements;
            var id = target.closest('tr').getAttribute('data-id');

            var userRecord = {};
            userRecord.id = id;
            userRecord.name = inputs.name.value;
            userRecord.email = inputs.email.value;
            userRecord.phone = inputs.phone.value;
            userRecord.street = inputs.street.value;
            userRecord.city = inputs.city.value;
            userRecord.state = inputs.state.value.toUpperCase();
            userRecord.zip = inputs.zip.value;

            myApp.view.removeEditUserForm();
            myApp.view.updateUserRow(userRecord);
            myApp.model.saveRecord(userRecord);
        } else
            alert('Please, fill all form fields correctly.');
    };

})(window.myApp = window.myApp || {}, jQuery);