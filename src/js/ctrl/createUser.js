(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // Create new user if form fields are filled correctly.
    ctrl.createUser = function() {
        if (ctrl.validateForm('addUserForm')) {
            var inputs = document.forms.addUserForm.elements;

            var userRecord = {};
            userRecord.id = new Date().getTime() + Math.round(Math.random() * 10000);
            userRecord.name = inputs.name.value;
            userRecord.email = inputs.email.value;
            userRecord.phone = inputs.phone.value;
            userRecord.street = inputs.street.value;
            userRecord.city = inputs.city.value;
            userRecord.state = inputs.state.value.toUpperCase();
            userRecord.zip = inputs.zip.value;

            myApp.view.removeAddUserForm();
            myApp.view.addUserRow(userRecord);
            myApp.model.saveRecord(userRecord);
            document.getElementById('add').disabled = false;
        } else
            alert('Please, fill all form fields correctly.');
    };

})(window.myApp = window.myApp || {}, jQuery);