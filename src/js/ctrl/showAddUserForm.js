(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // Display 'Add User' form.
    ctrl.showAddUserForm = function() {
        myApp.view.renderAddUserForm();
    };

})(window.myApp = window.myApp || {}, jQuery);