(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // Revert edit process.
    ctrl.revertEdit = function() {
        myApp.view.removeEditUserForm();
        myApp.view.updateUserRow(myApp.model.lastEditedRecord);
    };

})(window.myApp = window.myApp || {}, jQuery);