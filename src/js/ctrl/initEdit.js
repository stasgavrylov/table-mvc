(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // Initialize user editing process.
    ctrl.initEdit = function(target) {
        // Check if currently editing or creating other user record.
        if (document.querySelector('.edit'))
            ctrl.revertEdit();
        else if (document.querySelector('.create'))
            if (confirm('Are you sure you want to abort user creation process?'))
                myApp.view.removeAddUserForm();
            else
                return false;

        // Create user record object.
        var row = target.closest('tr');
        var id = row.getAttribute('data-id');

        var userRecord = myApp.model.UserRecord.instances[id];

        myApp.view.renderEditUserForm(userRecord, id);
        myApp.model.storeEditedRecord(userRecord);
    };

})(window.myApp = window.myApp || {}, jQuery);