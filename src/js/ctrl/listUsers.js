(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // List all user records.
    ctrl.listUsers = function() {
        var records = myApp.model.loadRecords();
        myApp.view.listUsers(records);
    };

})(window.myApp = window.myApp || {}, jQuery);