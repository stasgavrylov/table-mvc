(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // Create table row element with user data.
    ctrl.createRow = function(user) {
        var row = document.createElement('tr');
        var rowHTML = '';

        // InnerHTML here for brevity.
        rowHTML += '<td>' + user.name + '</td>';
        rowHTML += '<td>' + user.email + '</td>';
        rowHTML += '<td>' + user.phone + '</td>';
        rowHTML += '<td>' + user.street + '</td>';
        rowHTML += '<td>' + user.city + '</td>';
        rowHTML += '<td>' + user.state + '</td>';
        rowHTML += '<td>' + user.zip + '</td>';
        rowHTML += '<td><a href="#" title="Edit User" data-role="edit">';
        rowHTML += '<i class="glyphicon glyphicon-pencil"></i></a>';
        rowHTML += '<a href="#" title="Delete User" data-role="delete">';
        rowHTML += '<i class="glyphicon glyphicon-remove"></i></a></td>';

        row.setAttribute('data-id', user.id);
        row.innerHTML = rowHTML;

        return row;
    };

})(window.myApp = window.myApp || {}, jQuery);