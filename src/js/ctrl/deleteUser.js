(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // Delete user record.
    ctrl.deleteUser = function(target) {
        var confirmed = confirm('Do you really want to delete this record?');

        if (confirmed) {
            var row = target.closest('tr');
            var userId = row.getAttribute('data-id');
            
            myApp.model.deleteRecord(userId);
            myApp.view.removeUserRow(row);
        }
    };

})(window.myApp = window.myApp || {}, jQuery);