(function(myApp, $, undefined) {

    if (!(myApp.ctrl))
        myApp.ctrl = {};
    var ctrl = myApp.ctrl;

    // Basic check if all form fields are filled.
    ctrl.validateForm = function(name) {
        var form = document.forms[name];
        var valid = form.checkValidity();

        if (!valid) 
            switch(name) {
                case 'addUserForm':
                    document.querySelector('tr.create').className += ' validated';
                    break;
                case 'editUserForm':
                    document.querySelector('tr.edit').className += ' validated';
                    break;
            }
        return valid;
    };
    
})(window.myApp = window.myApp || {}, jQuery);