(function() {

    document.addEventListener('DOMContentLoaded', function() {
        // Load test data.
        myApp.model.loadTestData();

        // Init app.
        myApp.ctrl.listUsers();
    });

})();